var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var siteSchema = new Schema({
	url: String,
	port: Number,
	type: String,
	additionalData: Schema.Types.Mixed,
	status: String,
	downsince: Number,
	user: {
		type: Schema.Types.ObjectId,
		ref: "User"
	}
});

var Site = mongoose.model("Site", siteSchema);

module.exports = Site;