var express = require("express");
var router = express.Router();
var _ = require("underscore");
var Mongoose;
var User = require("../models/User.js");
var Site = require("../models/Site.js");

var validateError = function(req, params, cb) {
	var isValid = true;
	_.each(params, function(param) {
		if (_.isUndefined(req.body[param]) || req.body[param] === "" && param != "additionalData") isValid = false;
	});
	return !isValid;
};

router.use(function(req, res, next) {
	if (_.isUndefined(req.user)) {
		res.json({
			error: true,
			reason: "authenticate"
		}, 401);
	} else {
		next();
	}
});

router.get("/userInfos", function(req, res) {
	return res.json(req.user);
});

router.get("/sites", function(req, res, next) {
	Site.find({
		user: req.user
	}).populate("user").exec(function(err, sites) {
		if (err) {
			return next("dbError");
		}
		var newSites = [];
		_.each();
		res.json(sites);
	});
});

router.post("/addSite", function(req, res) {
	if (validateError(req, ["url", "port", "type", "additionalData"])) {
		return res.json({
			error: true,
			reason: "invalidRequest"
		});
	} else {
		var newSite = new Site();
		newSite.url = req.body.url;
		newSite.port = req.body.port;
		newSite.type = req.body.type;
		newSite.additionalData = req.body.additionalData;
		newSite.user = req.user;
		newSite.status = "none";
		newSite.downsince = 0;
		newSite.save(function(err, newSite)  {
			if (err) next(err);
			res.json({
				error: false
			});
		});
	}
});

router.post("/removeSite/:id", function(req, res, next)  {
	var id = req.params.id;
	if (_.isUndefined(id)) {
		return res.json({
			error: true,
			reason: "invalidRequest"
		});
	} else {
		Site.findOne({
			_id: id
		}).exec(function(err, site)  {
			if (err) return next(err);
			if (site.user.toString() != req.user._id.toString()) {
				return res.json({
					error: true,
					reason: "authenticate"
				}, 401);
			}
			Site.remove({
				_id: id
			}).exec(function(err)  {
				if (err) return next(err);
				return res.json({
					error: false
				});
			});
		});
	}
});

router.use(function(error, req, res) {
	return res.json({
		error: true,
		reason: "dbError"
	});
});

/* Export all routes here. */
module.exports = router;
// module.exports.init = function (Mongoose) {
// Mongoose = this.Mongoose;
// };