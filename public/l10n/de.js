{
	"COMMONS": {
		"WelcomeToYourpings": "Wilkommen bei Yourpings!"
	},
	"SITES": {
		"YourSites": "Deine Seiten",
		"Delete": "Löschen",
		"Method": "Methode",
		"Add": "Hinzufügen"
	},
	"LEGAL": {
		"Imprint": "Impressum"
	},
	"HEADER": {
		"Settings": "Einstellungen",
		"Logout": "Ausloggen",
		"Help": "Hilfe"
	},
	"NAV": {
		"Sites": "Seiten",
		"Dashboard": "Übersicht"
	}
}