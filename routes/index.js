var express = require("express");
var router = express.Router();
var _ = require("underscore");

/* Middleware */
router.get("/", function(req, res)  {
	if (!_.isUndefined(req.user))  {
		res.redirect("/dashboard");
	} {
		res.render("index");
	}
});

/* GET home page. */
router.get("/dashboard", function(req, res, next) {
	if (!_.isUndefined(req.user))  {
		res.sendFile("index.html", {
			root: __dirname + "/../public/"
		});
	} else {
		res.redirect("/auth/login");
	}
});

router.get("/dashboard", function(req, res, next) {
	res.render("dashboard", {
		user: req.user
	});
});

router.get("/throw", function(req, res) {
	process.exit(1);
});

module.exports = router;