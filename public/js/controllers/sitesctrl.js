app.controller("SitesController", ["$scope", "$interval", "Sites",
	function($scope, $interval, Sites) {
		//defaults
		$scope.newSiteMethod = "tcp";
		$scope.reloadData = function() {
			Sites.reloadData(function(sites) {
				$scope.sites = sites;
			});
		};
		$scope.reloadData();
		$scope.addSite = function() {
			Sites.addSite($scope.newSiteUrl, $scope.newSitePort, $scope.newSiteMethod, "", function() {
				$scope.reloadData();
				$scope.newSiteUrl = "";
				$scope.newSitePort = "";
				$scope.newSiteMethod = "tcp";
			});
		};
		$scope.removeSite = function(id)  {
			Sites.removeSite(id, function() {
				$scope.reloadData();
			});
		};
		var timer = $interval(
			function() {
				$scope.reloadData();
			},
			1000
		);
		$scope.$on(
			"$destroy",
			function(event) {
				$interval.cancel(timer);
			}
		);
	}
]);
app.directive('enterSubmit', function() {
	return {
		restrict: 'A',
		link: function(scope, elem, attrs) {
			elem.bind('keydown', function(event) {
				var code = event.keyCode || event.which;
				if (code === 13) {
					if (!event.shiftKey) {
						event.preventDefault();
						scope.$apply(attrs.enterSubmit);
					}
				}
			});
		}
	}
});