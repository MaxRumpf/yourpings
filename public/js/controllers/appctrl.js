app.controller("AppController", ["$scope", "User",
	function($scope, User)  {
		User.loadData(function(user)  {
			$scope.email = user.local.email;
		});
	}
]);