var express = require("express");
var router = express.Router();
var passport = require("passport");
var User = require("../models/User.js");

/* GET login page. */
router.get("/login", function(req, res) {
	res.render("auth/login", {
		message: req.flash('loginMessage')
	});
});

/* GET register page. */
router.get("/register", function(req, res, next) {
	res.render("auth/register", {
		message: req.flash('signupMessage')
	});
});

router.get("/logout", function(req, res) {
	req.session.destroy();
	req.logout();
	res.redirect("/");
});

/* POST login page. */
router.post("/login", passport.authenticate('local-login', {
	successRedirect: '/', // redirect to the secure profile section
	failureRedirect: '/auth/login', // redirect back to the signup page if there is an error
	failureFlash: true // allow flash messages
}), function(req, res) {
	res.redirect("/auth/aboutUser");
});


/* POST register page. */
router.post("/register", passport.authenticate('local-signup', {
	successRedirect: '/', // redirect to the secure profile section
	failureRedirect: '/auth/register', // redirect back to the signup page if there is an error
	failureFlash: true // allow flash messages
}));

/* Export all routes here. */
module.exports = router;