var passport = require("passport");
var LocalStrategy = require("passport-local").Strategy;
var UserModel = require("../models/User.js");
var _ = require("underscore");

passport.use('local-signup', new LocalStrategy({
		// by default, local strategy uses username and password, we will override with email
		usernameField: 'email',
		passwordField: 'password',
		passReqToCallback: true // allows us to pass back the entire request to the callback
	},
	function(req, email, password, done) {
		// asynchronous
		// User.findOne wont fire unless data is sent back
		if (_.isUndefined(req.body.name) || req.body.name === "") {
			return done(null, false, req.flash("signupMessage", "Please enter your name!"));
		} else if (_.isUndefined(req.body.password_confirmation) || req.body.password_confirmation === "" || req.body.password_confirmation != req.body.password) {
			return done(null, false, req.flash("signupMessage", "Please enter the same password twice."));
		} else {
			process.nextTick(function() {
				// find a user whose email is the same as the forms email
				// we are checking to see if the user trying to login already exists
				UserModel.findOne({
					'local.email': email
				}, function(err, user) {
					// if there are any errors, return the error
					if (err)
						return done(err);
					// check to see if theres already a user with that email
					if (user) {
						return done(null, false, req.flash('signupMessage', 'That email is already taken.'));
					} else {
						// if there is no user with that email
						// create the user
						var newUser = new UserModel();
						// set the user's local credentials
						newUser.local.email = email;
						newUser.local.password = newUser.generateHash(password);
						newUser.local.name = req.body.name;
						// save the user
						newUser.save(function(err) {
							if (err)
								throw err;
							return done(null, newUser);
						});
					}
				});
			});
		}
	}));
passport.use('local-login', new LocalStrategy({
		// by default, local strategy uses username and password, we will override with email
		usernameField: 'email',
		passwordField: 'password',
		passReqToCallback: true // allows us to pass back the entire request to the callback
	},
	function(req, email, password, done) { // callback with email and password from our form
		// find a user whose email is the same as the forms email
		// we are checking to see if the user trying to login already exists
		UserModel.findOne({
			'local.email': email
		}, function(err, user) {
			// if there are any errors, return the error before anything else
			if (err)
				return done(err);
			// if no user is found, return the message
			if (!user)
				return done(null, false, req.flash('loginMessage', 'No user found.')); // req.flash is the way to set flashdata using connect-flash
			// if the user is found but the password is wrong
			if (!user.validPassword(password))
				return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.')); // create the loginMessage and save it to session as flashdata
			// all is well, return successful user
			return done(null, user);
		});

	}));
passport.serializeUser(function(user, done) {
	done(null, user.id);
});
passport.deserializeUser(function(id, done) {
	UserModel.findById(id, function(err, user) {
		done(err, user);
	});
});

module.exports = passport;