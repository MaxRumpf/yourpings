var YourpingsService = angular.module("YourpingsService", []);

YourpingsService.service("Sites", ["$http",
	function($http) {

		this.reloadData = function(cb) {
			$http({
				method: "GET",
				url: "/api/sites"
			}).success(function(sites) {
				var newSites = [];
				sites.forEach(function(site) {
					site.up = site.status == "up";
					site.down = site.status == "down";
					newSites.push(site);
				});
				this.all_sites = newSites;
				cb(this.all_sites);
			}).error(function(data, status) {
				if (status == 401) {
					window.location.href = '/auth/login';
				} else {
					alert("$HTTP ERROR!!" + code.toString());
				}
			});
		};

		this.getSites = function() {
			return this.all_sites;
		};

		this.removeSite = function(id, cb)  {
			if (typeof id === "undefined") {
				return alert("Error deleting Site!!");
			}
			$http({
				method: "post",
				url: "/api/removeSite/" + id
			}).success(function(response)  {
				cb();
			}).error(function(response, status)  {
				alert("Error removing Site" + status.toString());
			});
		};

		this.addSite = function(url, port, method, additionalData, cb)  {
			$http({
				method: "post",
				url: "/api/addSite",
				data: {
					url: url,
					port: port,
					type: method,
					additionalData: additionalData
				}
			}).success(function(response) {
				cb();
			}).error(function(response, status)  {
				alert("Error creating Site " + status.toString());
			});
		};

	}
]);

YourpingsService.service("User", ["$http",
	function($http)  {
		this.loadData = function(cb) {
			$http({
				method: "get",
				url: "/api/userInfos"
			}).success(function(userInfos)  {
				cb(userInfos);
			});
		};
	}
]);