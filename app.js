var express = require("express");
var path = require("path");
var favicon = require("serve-favicon");
var logger = require("morgan");
var cookieParser = require("cookie-parser");
var bodyParser = require("body-parser");
var mongoose = require("mongoose");
var session = require("express-session");
var MongoStore = require("connect-mongo")(session);
var ConnectFlash = require("connect-flash");

var mongoDBConfig = require("./config/database.js");
mongoose.connect(mongoDBConfig.url, mongoDBConfig.options);

var db = mongoose.connection;
db.on("error", function(mongoError) {
	console.log("MongoDB Connection Error: " + mongoError);
	process.exit(1);
});

var app = express();

/* Providing Librarys in EJS-View */
app.locals._ = require("underscore");

app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

//app.use(favicon(path.join(__dirname, "public", "favicon.ico")));
app.use(logger("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: false
}));
app.use(cookieParser());

app.use(session({
	store: new MongoStore({
		mongooseConnection: db
	}),
	secret: "HliwuliaeblbelrwU)/986zGF97&%RiutzF(765E$678itugvr/(5r97FT"
}));
app.use(ConnectFlash());

/* Importing User Model. */


/* Passport stuff */
var passport = require("./config/passport.js");

app.use(passport.initialize());
app.use(passport.session());

app.use("/", require("./routes/index"));
app.use("/api", require("./routes/api"));
app.use("/auth", require("./routes/auth"));
app.use(express.static(path.join(__dirname, "public")));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
	var err = new Error("Not Found");
	err.status = 404;
	next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get("env") === "development") {
	app.use(function(err, req, res, next) {
		res.status(err.status || 500);
		res.render("error", {
			message: err.message,
			error: err
		});
	});
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
	res.status(err.status || 500);
	res.render("error", {
		message: err.message,
		error: {}
	});
});

module.exports = app;