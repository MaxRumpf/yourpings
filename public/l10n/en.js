{
	"COMMONS": {
		"WelcomeToYourpings": "Welcome to Yourpings!"
	},
	"SITES": {
		"YourSites": "Your Sites",
		"Delete": "Delete",
		"Method": "Method",
		"Add": "Add"
	},
	"LEGAL": {
		"Imprint": "Imprint"
	},
	"HEADER": {
		"Settings": "Settings",
		"Logout": "Logout",
		"Help": "Help"
	},
	"NAV": {
		"Sites": "Sites",
		"Dashboard": "Dashboard"
	}
}