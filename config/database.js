module.exports = {
	url: "mongodb://localhost/yourpings",
	options: {
		replset: {
			socketOptions: {
				keepAlive: 1
			}
		},
		server: {
			socketOptions: {
				keepAlive: 1
			}
		}
	}
};